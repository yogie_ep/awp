<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PHPMailer_library {

public function __construct()
{
log_message('Debug', 'PHPMailer class is loaded.');
}

public function load()
{
require_once('src/PHPMailer.php');
require_once('src/SMTP.php');

$objMail = new PHPMailer\PHPMailer\PHPMailer();
return $objMail;
}
}