<?php
Class Data extends CI_Controller{


	public function index(){

		$this->load->view("login_user");
	}

	function aksi_login(){
		$email = $this->input->post('email');
		$password = $this->input->post('password');
		$where = array(
			'email' => $email,
			'password' => md5($password)
			);
		$cek = $this->cek_login("userr",$where)->num_rows();
		if($cek > 0){
 
			$data_session = array(
				'email' => $email,
				'status' => "login"
				);
 
			$this->session->set_userdata($data_session);
 
			redirect(base_url("data/logged"));
 
		}else{
			echo "Username dan password salah !";
		}
	}

	function cek_login($table,$where){		
		return $this->db->get_where($table,$where);
	}	
 
	function logout(){
		$this->session->sess_destroy();
		redirect(base_url('data/index'));
	}


	public function logged(){
		$this->load->model("model_data");
		$data['list_user'] = $this->model_data->load_user();

		$this->load->view("data_user",$data);
	}

	public function add(){
		$this->load->model("model_data");
		$data['tipe'] = "Add";

		if(isset($_POST['tombol_submit'])){

			$this->model_data->simpan($_POST);
			redirect("data");
		}

		$this->load->view("form_user",$data);
	}

	

	public function edit($id){
		$this->load->model("model_data");
		$data['tipe'] = "Edit";
		$data['default'] = $this->model_data->get_default($id);

		if(isset($_POST['tombol_submit'])){
			$this->model_data->update($_POST, $id);
			redirect("data");
		}

		$this->load->view("form_user",$data);
	}


	public function delete($id){
		$this->load->model("model_data");
		$this->model_data->hapus($id);
		redirect("data");
	}



}