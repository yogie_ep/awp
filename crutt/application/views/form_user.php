<!doctype html>
<html lang="en">
<head>
	<base href="<?=base_url()?>">
	<meta charset="UTF-8">
	<title>CRUD</title>
	<link rel="stylesheet" href="css/style.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
	<script src='https://www.google.com/recaptcha/api.js'></script>
</head>
<body>
	<div class="container">
		<h1><?=$tipe?> User</h1>
		
		<form method="post" class="form-horizontal">
			<div class="form-group">
				<label class="control-label col-sm-2">
					Nama
				</label>
				<div class="col-sm-10">
					<input type="text" class="form-control" name="nama" value="<?=isset($default['name'])? $default['name'] : ""?>">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-2">
					Password
				</label>
				<div class="col-sm-10">
					<input type="password" class="form-control" name="password" value="<?=isset($default['password'])? $default['password'] : ""?>">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-2">
					Email					
				</label>
				<div class="col-sm-10">
					<input type="email" class="form-control" name="email" value="<?=isset($default['email'])? $default['email'] : ""?>">
				</div>
			</div>
			<center>
			<div class="g-recaptcha" data-sitekey="6LdOgasUAAAAAFwGunJB6OjOb4gzbr9a9Tq-88NH"></div>
			<br>
			<br>
				<button name="tombol_submit" class="btn btn-primary">
					Simpan
				</button>
				<br>
			</center>


		</form>
	</div>
</body>
</html>